﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swapi.Interfaces
{
    public interface ICallable
    {
        string Url { get; set; }
        string GetPoster { get; }
        int GetId();
        string GetMod();
        string GetPosterData();

    }
}
