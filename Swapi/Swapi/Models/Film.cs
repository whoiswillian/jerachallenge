﻿using Swapi.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Swapi.Models
{
    public class Film : ICallable
    {
        public string Title { get; set; }
        public int Episode_id { get; set; }
        public string Opening_crawl { get; set; }
        public string Director { get; set; }
        public string Producer { get; set; }
        public string Release_date { get; set; }
        public List<string> characters { get; set; }
        public IEnumerable<Person> Characters { get; set; }
        public List<string> planets { get; set; }
        public IEnumerable<Planet> Planets { get; set; }
        public List<string> starships { get; set; }
        public IEnumerable<Starship> Starships { get; set; }
        public List<string> vehicles { get; set; }
        public IEnumerable<Vehicle> Vehicles { get; set; }
        public List<string> species { get; set; }
        public IEnumerable<Specie> Species { get; set; }
        public DateTime Created { get; set; }
        public DateTime Edited { get; set; }
        public string Url { get; set; }
        public string GetPoster => GetPosterData();

        public int GetId()
        {
            var list = Url.Split('/');
            return int.Parse(list[5]);
        }
        public string GetMod()
        {
            var list = Url.Split('/');
            return list[4];
        }

        public string GetPosterData()
        {
            return "https://jerachallenge.azurewebsites.net/images/" + GetMod() + "" + GetId() + ".jpg";
        }
    }
}
