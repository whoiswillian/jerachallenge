﻿using Swapi.Interfaces;
using System;

namespace Swapi.Models
{
    public class Item : ICallable
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string GetPoster => GetPosterData();

        public int GetId()
        {
            var list = Url.Split('/');
            return int.Parse(list[5]);
        }
        public string GetMod()
        {
            var list = Url.Split('/');
            return list[4];
        }

        public string GetPosterData()
        {
            return "https://jerachallenge.azurewebsites.net/images/" + GetMod() + "" + GetId() + ".jpg";
        }
    }
}