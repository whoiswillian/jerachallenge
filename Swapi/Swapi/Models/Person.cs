﻿using Swapi.Interfaces;
using Swapi.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Swapi.Models
{
    public class Person : ICallable
    {
        public string Name { get; set; }
        public string Height { get; set; }
        public string Mass { get; set; }
        public string Hair_color { get; set; }
        public string Skin_color { get; set; }
        public string Eye_color { get; set; }
        public string Birth_year { get; set; }
        public string Gender { get; set; }
        public string homeworld { get; set; }
        public Planet Homeworld { get; set; }
        public List<string> films { get; set; }
        public IEnumerable<Film> Films { get; set; }
        public List<string> species { get; set; }
        public IEnumerable<Specie> Species { get; set; }
        public List<object> vehicles { get; set; }
        public IEnumerable<Vehicle> Vehicles { get; set; }
        public List<object> starships { get; set; }
        public IEnumerable<Starship> Starships { get; set; }
        public DateTime Created { get; set; }
        public DateTime Edited { get; set; }
        public string Url { get; set; }

        public string GetPoster => GetPosterData();

        public int GetId()
        {
            var list = Url.Split('/');
            return int.Parse(list[5]);
        }
        public string GetMod()
        {
            var list = Url.Split('/');
            return list[4];
        }

        public string GetPosterData()
        {
            return "https://jerachallenge.azurewebsites.net/images/" + GetMod() + "" + GetId() + ".jpg";
        }
    }
}
