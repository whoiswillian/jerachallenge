﻿using Swapi.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Swapi.Models
{
    public class Planet : ICallable
    {
        public string Name { get; set; }
        public string Rotation_period { get; set; }
        public string Orbital_period { get; set; }
        public string Diameter { get; set; }
        public string Climate { get; set; }
        public string Gravity { get; set; }
        public string Terrain { get; set; }
        public string Surface_water { get; set; }
        public string Population { get; set; }
        public List<string> residents { get; set; }
        public IEnumerable<Person> Residents { get; set; }
        public List<string> films { get; set; }
        public IEnumerable<Film> Films { get; set; }
        public DateTime Created { get; set; }
        public DateTime Edited { get; set; }
        public string Url { get; set; }

        public string GetPoster => GetPosterData();

        public int GetId()
        {
            var list = Url.Split('/');
            return int.Parse(list[5]);
        }
        public string GetMod()
        {
            var list = Url.Split('/');
            return list[4];
        }

        public string GetPosterData()
        {
            return "https://jerachallenge.azurewebsites.net/images/" + GetMod() + "" + GetId() + ".jpg";
        }
    }
}
