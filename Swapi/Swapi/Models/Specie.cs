﻿using Swapi.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Swapi.Models
{
    public class Specie : ICallable
    {
        public string Name { get; set; }
        public string Classification { get; set; }
        public string Designation { get; set; }
        public string Average_height { get; set; }
        public string Skin_colors { get; set; }
        public string Hair_colors { get; set; }
        public string Eye_colors { get; set; }
        public string Average_lifespan { get; set; }
        public string homeworld { get; set; }
        public Planet Homeworld { get; set; }
        public string Language { get; set; }
        public List<string> people { get; set; }
        public IEnumerable<Person> People { get; set; }
        public List<string> films { get; set; }
        public IEnumerable<Film> Films { get; set; }
        public DateTime Created { get; set; }
        public DateTime Edited { get; set; }
        public string Url { get; set; }
        public string GetPoster => GetPosterData();

        public int GetId()
        {
            var list = Url.Split('/');
            return int.Parse(list[5]);
        }
        public string GetMod()
        {
            var list = Url.Split('/');
            return list[4];
        }

        public string GetPosterData()
        {
            return "https://jerachallenge.azurewebsites.net/images/" + GetMod() + "" + GetId() + ".jpg";
        }
    }
}
