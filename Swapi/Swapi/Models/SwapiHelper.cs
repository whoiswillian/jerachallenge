﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Swapi.Models
{
    public class SwapiHelper<ICallable>
    {
        HttpClient client;
        string module;
        public int Count { get; set; }
        public string Next { get; set; }
        public object Previous { get; set; }
        public List<ICallable> Results { get; set; }
        
        public SwapiHelper(string _module)
        {
            module = _module;
            client = new HttpClient();
            client.BaseAddress = new Uri($"https://swapi.co/");
        }
        public async Task<IEnumerable<Typ>> GetAllAsync<Typ>()
        {
            var json = await client.GetStringAsync($"api/{module}");
            var temp = await Task.Run(() => JsonConvert.DeserializeObject<SwapiHelper<Typ>>(json));
            List<Typ> items = temp.Results;
            var page = 2;
            while (temp.Next != null)
            {
                json = await client.GetStringAsync($"api/{module}/?page=" + page);
                temp = await Task.Run(() => JsonConvert.DeserializeObject<SwapiHelper<Typ>>(json));
                foreach (var item in temp.Results)
                {
                    items.Add(item);
                }
                page += 1;
            }
            return items;
        }
        public async Task<IEnumerable<Typ>> GetParentAsync<Typ>(string parent)
        {
            var json = await client.GetStringAsync($"api/{parent}");
            var temp = await Task.Run(() => JsonConvert.DeserializeObject<SwapiHelper<Typ>>(json));
            List<Typ> items = temp.Results;
            var page = 2;
            while(temp.Next != null)
            {
                json = await client.GetStringAsync($"api/{parent}/?page="+page);
                temp = await Task.Run(() => JsonConvert.DeserializeObject<SwapiHelper<Typ>>(json));
                foreach(var item in temp.Results)
                {
                    items.Add(item);
                }
                page+=1;
            }
            return items;
        }
    }
}
