﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Swapi.Interfaces;
using Swapi.Models;

namespace Swapi.Services
{
    public class AzureDataStore : IDataStore<ICallable>
    {
        HttpClient client;
        IEnumerable<ICallable> items;
        public string module { get; set; }

        public AzureDataStore()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri($"{App.AzureBackendUrl}/");
            items = new List<ICallable>();
        }

        public async Task<IEnumerable<ICallable>> GetItemsAsync(bool forceRefresh = false)
        {
            if (forceRefresh)
            {
                var json = await client.GetStringAsync($"api/{module}");
                var temp = await Task.Run(() => JsonConvert.DeserializeObject<SwapiHelper<Film>>(json));
                items = temp.Results;
            }

            return items;
        }

        public async Task<ICallable> GetItemAsync(string id)
        {
            if (id != null)
            {
                var json = await client.GetStringAsync($"api/{module}/{id}");
                return await Task.Run(() => JsonConvert.DeserializeObject<Film>(json));
            }

            return null;
        }
    }
}