﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swapi.Interfaces;
using Swapi.Models;

namespace Swapi.Services
{
    public class MockDataStore : IDataStore<ICallable>
    {
        List<ICallable> items;
        public string module { get; set; }

        public MockDataStore()
        {
            items = new List<ICallable>();
            var mockItems = new List<Item>
            {
                new Item { Id = Guid.NewGuid().ToString(), Text = "First item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Second item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Third item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Fourth item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Fifth item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Sixth item", Description="This is an item description." },
            };

            foreach (var item in mockItems)
            {
                items.Add(item);
            }
        }

        public async Task<bool> AddItemAsync(ICallable item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(ICallable item)
        {
            var oldItem = items.Where((ICallable arg) => arg.Url == item.Url).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((ICallable arg) => arg.Url == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<ICallable> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Url == id));
        }

        public async Task<IEnumerable<ICallable>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}