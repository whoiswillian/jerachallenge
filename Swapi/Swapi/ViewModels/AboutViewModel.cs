﻿using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace Swapi.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "About";

            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("https://jerachallenge.azurewebsites.net")));
        }

        public ICommand OpenWebCommand { get; }
    }
}