﻿using Swapi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Swapi.ViewModels
{
    public class CharsDetailViewModel : BaseViewModel
    {
        public Person Char { get; set; }
        public CharsDetailViewModel(Person character = null)
        {
            Title = character?.Name;
            Char = character;
        }
    }
}
