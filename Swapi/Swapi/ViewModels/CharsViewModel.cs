﻿using Swapi.Interfaces;
using Swapi.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Swapi.ViewModels
{
    public class CharsViewModel : BaseViewModel
    {
        public ObservableCollection<ICallable> Chars { get; set; }
        public Command LoadCharsCommand { get; set; }
        public SwapiHelper<Person> SwapiHelper { get; set; }

        public CharsViewModel()
        {
            Title = "Characters";
            Chars = new ObservableCollection<ICallable>();
            LoadCharsCommand = new Command(async () => await ExecuteLoadCharsCommand());
            SwapiHelper = new SwapiHelper<Person>("people");
        }

        async Task ExecuteLoadCharsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Chars.Clear();
                var items = await SwapiHelper.GetAllAsync<Person>();
                var films = await SwapiHelper.GetParentAsync<Film>("films");
                var planet = await SwapiHelper.GetParentAsync<Planet>("planets");
                var specie = await SwapiHelper.GetParentAsync<Specie>("species");
                var startship = await SwapiHelper.GetParentAsync<Starship>("starships");
                var vehicle = await SwapiHelper.GetParentAsync<Vehicle>("vehicles");
                foreach (var item in items)
                {
                    item.Films = films.Where(c => item.films.Contains(c.Url));
                    item.Homeworld = planet.First(p => item.homeworld == p.Url);
                    item.Species = specie.Where(sp => item.species.Contains(sp.Url));
                    item.Starships = startship.Where(st => item.starships.Contains(st.Url));
                    item.Vehicles = vehicle.Where(st => item.vehicles.Contains(st.Url));
                    Chars.Add(item);

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
