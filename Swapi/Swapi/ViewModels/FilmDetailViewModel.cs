﻿using Swapi.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Swapi.ViewModels
{
    public class FilmDetailViewModel : BaseViewModel
    {
        public Film Film { get; set; }
        public FilmDetailViewModel(Film film = null)
        {
            Title = film?.Title;
            Film = film;
        }
    }
}
