﻿using Swapi.Interfaces;
using Swapi.Models;
using Swapi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Swapi.ViewModels
{
    public class FilmsViewModel : BaseViewModel
    {
        public ObservableCollection<ICallable> Films { get; set; }
        public Command LoadFilmsCommand { get; set; }
        public SwapiHelper<Film> SwapiHelper { get; set; }

        public FilmsViewModel()
        {
            Title = "Films";
            Films = new ObservableCollection<ICallable>();
            LoadFilmsCommand = new Command(async () => await ExecuteLoadFilmsCommand());
            SwapiHelper = new SwapiHelper<Film>("films");
        }

        async Task ExecuteLoadFilmsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Films.Clear();
                var items = await SwapiHelper.GetAllAsync<Film>();
                var person = await SwapiHelper.GetParentAsync<Person>("people");
                var planet = await SwapiHelper.GetParentAsync<Planet>("planets");
                var specie = await SwapiHelper.GetParentAsync<Specie>("species");
                var startship = await SwapiHelper.GetParentAsync<Starship>("starships");
                var vehicle = await SwapiHelper.GetParentAsync<Vehicle>("vehicles");
                foreach (var item in items.OrderBy(x => x.Episode_id))
                {
                    item.Characters = person.Where(c => item.characters.Contains(c.Url));
                    item.Planets = planet.Where(p => item.planets.Contains(p.Url));
                    item.Species = specie.Where(sp => item.species.Contains(sp.Url));
                    item.Starships = startship.Where(st => item.starships.Contains(st.Url));
                    item.Vehicles = vehicle.Where(st => item.vehicles.Contains(st.Url));
                    Films.Add(item);

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}