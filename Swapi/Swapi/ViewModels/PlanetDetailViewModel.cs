﻿using Swapi.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Swapi.ViewModels
{
    public class PlanetDetailViewModel : BaseViewModel
    {
        public Planet Planet { get; set; }
        public PlanetDetailViewModel(Planet specie = null)
        {
            Title = specie?.Name;
            Planet = specie;
        }
    }
}
