﻿using Swapi.Interfaces;
using Swapi.Models;
using Swapi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Swapi.ViewModels
{
    public class PlanetsViewModel : BaseViewModel
    {
        public ObservableCollection<ICallable> Planets { get; set; }
        public Command LoadPlanetCommand { get; set; }
        public SwapiHelper<Specie> SwapiHelper { get; set; }

        public PlanetsViewModel()
        {
            Title = "Planets";
            Planets = new ObservableCollection<ICallable>();
            LoadPlanetCommand = new Command(async () => await ExecuteLoadPlanetsCommand());
            SwapiHelper = new SwapiHelper<Specie>("planets");
        }

        async Task ExecuteLoadPlanetsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Planets.Clear();
                var items = await SwapiHelper.GetAllAsync<Planet>();
                var residents = await SwapiHelper.GetParentAsync<Person>("people");
                var films = await SwapiHelper.GetParentAsync<Film>("films");
                foreach (var item in items)
                {
                    item.Residents = residents.Where(c => item.residents.Contains(c.Url));
                    item.Films = films.Where(p => item.films.Contains(p.Url));
                    Planets.Add(item);

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}