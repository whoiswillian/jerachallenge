﻿using Swapi.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Swapi.ViewModels
{
    public class SpecieDetailViewModel : BaseViewModel
    {
        public Specie Specie { get; set; }
        public SpecieDetailViewModel(Specie specie = null)
        {
            Title = specie?.Name;
            Specie = specie;
        }
    }
}
