﻿using Swapi.Interfaces;
using Swapi.Models;
using Swapi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Swapi.ViewModels
{
    public class SpeciesViewModel : BaseViewModel
    {
        public ObservableCollection<ICallable> Species { get; set; }
        public Command LoadSpecieCommand { get; set; }
        public SwapiHelper<Specie> SwapiHelper { get; set; }

        public SpeciesViewModel()
        {
            Title = "Films";
            Species = new ObservableCollection<ICallable>();
            LoadSpecieCommand = new Command(async () => await ExecuteLoadSpecieCommand());
            SwapiHelper = new SwapiHelper<Specie>("species");
        }

        async Task ExecuteLoadSpecieCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Species.Clear();
                var items = await SwapiHelper.GetAllAsync<Specie>();
                var planets = await SwapiHelper.GetParentAsync<Planet>("planets");
                var person = await SwapiHelper.GetParentAsync<Person>("people");
                var films = await SwapiHelper.GetParentAsync<Film>("films");
                foreach (var item in items)
                {
                    item.Homeworld = planets.First(c => item.homeworld == c.Url);
                    item.People = person.Where(c => item.people.Contains(c.Url));
                    item.Films = films.Where(p => item.films.Contains(p.Url));
                    Species.Add(item);

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}