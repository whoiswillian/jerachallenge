﻿using Swapi.Models;
using Swapi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Swapi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CharDetailPage : ContentPage
    {
        CharsDetailViewModel viewModel;
        public CharDetailPage(CharsDetailViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;
            var k = 0;

            // Defining the Film data
            GridFilm.Padding = 3;
            GridFilm.WidthRequest = 110 * viewModel.Char.films.Count;
            GridFilm.HeightRequest = 150;
            k = 0;
            GridFilm.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Char.Films)
            {
                GridFilm.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {   
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Title,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridFilm.Children.Add(frame);
                k = k + 1;
            }


            // Defining the planets data
            GridPlanet.Padding = 3;
            GridPlanet.HeightRequest = 150;
            k = 0;
            GridPlanet.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            List<Planet> home = new List<Planet>();
            home.Add(viewModel.Char.Homeworld);
            GridPlanet.WidthRequest = 110 * home.Count;
            foreach (var item in home)
            {
                GridPlanet.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridPlanet.Children.Add(frame);
                k = k + 1;
            }

            // Defining the Specie data
            GridSpecie.Padding = 3;
            GridSpecie.WidthRequest = 110 * viewModel.Char.species.Count;
            GridSpecie.HeightRequest = 150;
            k = 0;
            GridSpecie.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Char.Species)
            {
                GridSpecie.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridSpecie.Children.Add(frame);
                k = k + 1;
            }

            // Defining the Starship data
            GridStarship.Padding = 3;
            GridStarship.WidthRequest = 110 * viewModel.Char.starships.Count;
            GridStarship.HeightRequest = 150;
            k = 0;
            GridStarship.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Char.Starships)
            {
                GridStarship.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridStarship.Children.Add(frame);
                k = k + 1;
            }

            // Defining the Vehicle data
            GridVehicles.Padding = 3;
            GridVehicles.WidthRequest = 110 * viewModel.Char.vehicles.Count;
            GridVehicles.HeightRequest = 150;
            k = 0;
            GridVehicles.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Char.Vehicles)
            {
                GridVehicles.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridVehicles.Children.Add(frame);
                k = k + 1;
            }
        }
        public CharDetailPage()
        {
            InitializeComponent();
            var Char = new Person
            {
                Name = "The Journey",
                Birth_year = "Continue"
            };
            viewModel = new CharsDetailViewModel(Char);
            BindingContext = viewModel;
        }
    }
}