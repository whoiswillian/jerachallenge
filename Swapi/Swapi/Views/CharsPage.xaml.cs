﻿using Swapi.Models;
using Swapi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Swapi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CharsPage : ContentPage
    {
        CharsViewModel viewModel;
        public CharsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new CharsViewModel();
        }
        async void OnCharSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var character = args.SelectedItem as Person;
            if (character == null)
                return;

            await Navigation.PushAsync(new CharDetailPage(new CharsDetailViewModel(character)));

            // Manually deselect item.
            CharsListView.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Chars.Count == 0)
                viewModel.LoadCharsCommand.Execute(null);
        }
    }
}