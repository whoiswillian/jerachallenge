﻿using Swapi.Models;
using Swapi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Swapi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FilmDetailPage : ContentPage
    {
        FilmDetailViewModel viewModel;
        public FilmDetailPage(FilmDetailViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;

            // Defining the hero data
            GridHero.Padding = 3;
            GridHero.WidthRequest = 110 * viewModel.Film.characters.Count;
            GridHero.HeightRequest = 150;
            var k = 0;
            GridHero.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Film.Characters)
            {
                GridHero.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                           new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridHero.Children.Add(frame);
                k = k + 1;
            }

            // Defining the planets data
            GridPlanet.Padding = 3;
            GridPlanet.WidthRequest = 110 * viewModel.Film.planets.Count;
            GridPlanet.HeightRequest = 150;
            k = 0;
            GridPlanet.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Film.Planets)
            {
                GridPlanet.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridPlanet.Children.Add(frame);
                k = k + 1;
            }

            // Defining the Specie data
            GridSpecie.Padding = 3;
            GridSpecie.WidthRequest = 110 * viewModel.Film.species.Count;
            GridSpecie.HeightRequest = 150;
            k = 0;
            GridSpecie.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Film.Species)
            {
                GridSpecie.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridSpecie.Children.Add(frame);
                k = k + 1;
            }

            // Defining the Starship data
            GridStarship.Padding = 3;
            GridStarship.WidthRequest = 110 * viewModel.Film.starships.Count;
            GridStarship.HeightRequest = 150;
            k = 0;
            GridStarship.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Film.Starships)
            {
                GridStarship.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridStarship.Children.Add(frame);
                k = k + 1;
            }

            // Defining the Vehicle data
            GridVehicles.Padding = 3;
            GridVehicles.WidthRequest = 110 * viewModel.Film.vehicles.Count;
            GridVehicles.HeightRequest = 150;
            k = 0;
            GridVehicles.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Film.Vehicles)
            {
                GridVehicles.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridVehicles.Children.Add(frame);
                k = k + 1;
            }
        }
        public FilmDetailPage()
        {
            InitializeComponent();
            var film = new Film
            {
                Title = "The Journey",
                Opening_crawl = "Continue"
            };
            viewModel = new FilmDetailViewModel(film);
            BindingContext = viewModel;
        }
    }
}