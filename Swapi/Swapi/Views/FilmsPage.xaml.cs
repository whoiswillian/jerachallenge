﻿using Swapi.Models;
using Swapi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Swapi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FilmsPage : ContentPage
    {
        FilmsViewModel viewModel;
        public FilmsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new FilmsViewModel();
        }
        async void OnFilmSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var film = args.SelectedItem as Film;
            if (film == null)
                return;

            await Navigation.PushAsync(new FilmDetailPage(new FilmDetailViewModel(film)));

            // Manually deselect item.
            FilmsListView.SelectedItem = null;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Films.Count == 0)
                viewModel.LoadFilmsCommand.Execute(null);
        }
    }
}