﻿using Swapi.Models;
using Swapi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Swapi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlanetsPage : ContentPage
    {
        PlanetsViewModel viewModel;
        public PlanetsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new PlanetsViewModel();
        }
        async void OnFilmSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var planet = args.SelectedItem as Planet;
            if (planet == null)
                return;

            await Navigation.PushAsync(new PlanetDetailPage(new PlanetDetailViewModel(planet)));

            // Manually deselect item.
            PlanetsListView.SelectedItem = null;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Planets.Count == 0)
                viewModel.LoadPlanetCommand.Execute(null);
        }
    }
}