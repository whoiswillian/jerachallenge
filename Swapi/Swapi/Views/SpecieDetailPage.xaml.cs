﻿using Swapi.Models;
using Swapi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Swapi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpecieDetailPage : ContentPage
    {
        SpecieDetailViewModel viewModel;
        public SpecieDetailPage(SpecieDetailViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;

            // Defining the planets data
            GridPlanet.Padding = 3;
            GridPlanet.HeightRequest = 150;
            var k = 0;
            GridPlanet.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            List<Planet> home = new List<Planet>();
            home.Add(viewModel.Specie.Homeworld);
            GridPlanet.WidthRequest = 110 * home.Count;
            foreach (var item in home)
            {
                GridPlanet.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridPlanet.Children.Add(frame);
                k = k + 1;
            }

            // Defining the hero data
            GridHero.Padding = 3;
            GridHero.WidthRequest = 110 * viewModel.Specie.people.Count;
            GridHero.HeightRequest = 150;
            k = 0;
            GridHero.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Specie.People)
            {
                GridHero.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                           new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Name,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridHero.Children.Add(frame);
                k = k + 1;
            }

            // Defining the Film data
            GridFilm.Padding = 3;
            GridFilm.WidthRequest = 110 * viewModel.Specie.films.Count;
            GridFilm.HeightRequest = 150;
            k = 0;
            GridFilm.RowDefinitions.Add(new RowDefinition { Height = new GridLength(145, GridUnitType.Absolute) });
            foreach (var item in viewModel.Specie.Films)
            {
                GridFilm.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) });
                Frame frame = new Frame
                {
                    Padding = 0,
                    HasShadow = true,
                    HeightRequest = 140,
                    WidthRequest = 100,
                    Content = new StackLayout
                    {
                        Children =
                        {
                            new Image{
                                Margin = 0,
                                Source = ImageSource.FromUri(new Uri(item.GetPoster)),
                                HeightRequest = 100,
                                HorizontalOptions = LayoutOptions.Center
                            },
                            new Label
                            {
                                Text = item.Title,
                                FontSize = 11
                            }
                        }
                    },
                };
                Grid.SetRow(frame, 0);
                Grid.SetColumn(frame, k);
                GridFilm.Children.Add(frame);
                k = k + 1;
            }
        }
        public SpecieDetailPage()
        {
            InitializeComponent();
            var specie = new Specie
            {
                Name = "The Journey",
                Language = "Continue"
            };
            viewModel = new SpecieDetailViewModel(specie);
            BindingContext = viewModel;
        }
    }
}