﻿using Swapi.Models;
using Swapi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Swapi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpeciesPage : ContentPage
    {
        SpeciesViewModel viewModel;
        public SpeciesPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new SpeciesViewModel();
        }
        async void OnFilmSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var specie = args.SelectedItem as Specie;
            if (specie == null)
                return;

            await Navigation.PushAsync(new SpecieDetailPage(new SpecieDetailViewModel(specie)));

            // Manually deselect item.
            SpeciesListView.SelectedItem = null;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Species.Count == 0)
                viewModel.LoadSpecieCommand.Execute(null);
        }
    }
}