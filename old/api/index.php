<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	require "../controller/BookController.php";
	require"../controller/ReminderController.php";
	require_once "../model/Book.php";
	require_once "../model/Reminder.php";
	$cBook = new BookController();
	$cReminder = new ReminderController();
	if(isset($_POST["action"])){
		$result = array("message" => "Nenhuma ação finalizada");
		$obj = isset($_POST["name"])?new Book(null, $_POST["name"], $_POST["category"], $_POST["pages"], $_POST["img"]): new Reminder(null, $_POST["hour"], $_POST["day"], $_POST["bookId"]);		
		switch($_POST["action"]){
			case "insert_book":
				if(isset($_POST["name"]) && isset($_POST["category"]) && isset($_POST["pages"])){
					$result = $cBook->CreateBook($obj);
				}else{
					$result = array("message" => "Faltam parâmetros");
				}
			break;
			case "update_book":
				$result = $cbook->UpdateBook($obj);
			break;
			case "insert_reminder":
				if(isset($_POST["hour"]) && isset($_POST["day"]) && isset($_POST["bookId"])){
					$result = $cReminder->CreateReminder($obj);
				}else{
					$result = array("message" => "Faltam parâmetros");
				}
			break;
			case "update_reminder":
				$result = $cReminder->UpdateReminder($obj);
			break;
		}
		exit(json_encode($result));
	}else if(isset($_GET["action"])){
		$result = array("message" => "Nenhuma ação finalizada");
		switch($_GET["action"]){
			case "get_book_list":
				$result = $cBook->ReadBooks();
			break;
			case "get_book_by_id":
				$result = $cBook->ReadBookById($_GET["id"]);
			break;
			case "get_reminder_list":
				$result = $cReminder->ReadReminders();
			break;
			case "get_reminder_by_id":
				$result = $cReminder->ReadReminderById($_GET["id"]);
			break;
			case "get_reminder_list_by_book_id":
				$result = $cReminder->ReadReminderByBookId($_GET["id"]);
			break;
			case "delete_book":
				$result = $cBook->DeleteBook($_GET["id"]);
			break;
			case "delete_reminder":
				$result = $cReminder->DeleteReminder($_GET["id"]);
			break;
		}
		exit(json_encode($result));
	}
?>