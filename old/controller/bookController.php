<?php
	require "../model/Book.php";
	class BookController{
		function CreateBook($obj){
			return $obj->InsertBook($obj);
		}
		function ReadBooks(){
			$book = new Book();
			return $book->GetBooks();
		}
		function ReadBookById($id){
			$book = new Book();
			return $book->GetBookById($id);
		}
		function UpdateBook($obj){
			$obj->UpdateBook($obj);
		}
		function DeleteBook($id){
			$book = new Book();
			$book->DeleteBook($id);
		}
	}
?>