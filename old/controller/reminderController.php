<?php
	require "../model/Reminder.php";
	class ReminderController{
		function CreateReminder($obj){
			$reminder = new Reminder();
			return $reminder->InsertReminder($obj);
		}
		function ReadReminders(){
			$reminder = new Reminder();
			return $reminder->GetReminders();
		}
		function ReadReminderById($id){
			$reminder = new Reminder();
			return $reminder->GetReminderById($id);
		}
		function ReadReminderByBookId($id){
			$reminder = new Reminder();
			if($reminder->GetReminderByBookId($id)){
				return array("message" => "Deletado com sucesso");
			}else{
				return array("message" => "Tente novamente");
			}
		}
		function UpdateReminder($obj){
			$obj->UpdateReminder($obj);
		}
		function DeleteReminder($id){
			$reminder = new Reminder();
			$reminder->DeleteReminder($id);
		}
	}
?>