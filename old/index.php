<!DOCTYPE html>
<html>
	<head>
		<title>MyBooks</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
		<link rel="stylesheet" href="content/min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<script src="scripts/app.js"></script>
	</head>
	<body>
		<?php 
			include "view/principal.php";
		?>
		<!-- Mensagem Modal - display as alert() -->
		<div id="messageModal" class="w3-modal">
		    <div class="w3-modal-content w3-card-4">
		      <header class="w3-container w3-teal"> 
		        <span class="w3-btn w3-teal" onclick="document.getElementById('id01').style.display='none'" 
		        class="w3-button w3-display-topright">&times;</span>
		        <h2 id="alertTitle"></h2>
		      </header>
		      <div class="w3-container">
		        <div id="alertContent"></div>
		      </div>
		      <footer class="w3-container w3-teal">
		        <div class="w3-btn w3-col l12 m12 s12" id="okText"></div>
		      </footer>
		    </div>
		  </div>
		<!--  -->
		<div id="itensModal" class="w3-modal w3-center">
		    <div class="w3-modal-content w3-card-4">
		      <header class="w3-container w3-teal"> 
		        <h2 id="itensModalTitle"></h2>
		      </header>
		      <div class="w3-container">
		        <div id="itensModalBody"></div>
		      </div>
		      <footer class="w3-teal">
		        <div class="w3-btn w3-col l12 m12 s12" id="itensModalOkText" onclick="document.getElementById('itensModal').style.display='none'">Ok</div>
		      </footer>
		    </div>
		  </div>
		<!-- Insert Reminder Modal - display a form to add reminder -->
		<!-- Modal de inclusão de lembrete - mostra um formulário para a inclusão -->
		<div id="insertModal" class="w3-modal">
		    <div class="w3-modal-content w3-card-4">
		      <header class="w3-container w3-indigo"> 
		        <span class="w3-btn w3-display-topright w3-hover-red" onclick="document.getElementById('insertModal').style.display='none'">&times;</span>
		        <h2 class="w3-center">Inserir Lembrete</h2>
		      </header>
		      <div class="w3-indigo">
		        <?php
					include "view/insertReminder.php";
				?>
		      </div>
		    </div>
		  </div>
	</body>
</html>