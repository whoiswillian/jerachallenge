<?php
class Book{
	public $id;
	public $name;
	public $category;
	public $pages;
	public $img;
	
	function __construct(){ 
        $a = func_get_args(); 
        $i = func_num_args(); 
        if (method_exists($this,$f='__construct'.$i)) { 
            call_user_func_array(array($this,$f),$a); 
        } 
    } 
	
	public function __construct5($id, $name, $category, $pages, $img){
		$this->id = $id;
		$this->name = $name;
		$this->category = $category;
		$this->pages = $pages;
		$this->img = $img;
	}
	
	public function __get($name) {
        switch (strtolower($name)){
            case 'id':
                return $this->id;
			break;
            case 'name':
                return $this->name;
			break;
            case 'category':
                return $this->category;
			break;
            case 'pages':
                return $this->pages;
			break;
            case 'img':
                return $this->img;
			break;
        }
	}
	public function __set($name, $value) {
        switch (strtolower($name)){
            case 'name':
                $this->name = $value;
			break;
            case 'category':
                $this->category = $value;
			break;
			 case 'pages':
                $this->pages = $value;
			break;
			 case 'img':
                $this->img = $value;
			break;
        }
    }
	
	public function InsertBook($obj){
		$bd = $this->Connect();
		$bd->query("INSERT INTO book(name, category, pages, img) VALUES('$obj->name','$obj->category',$obj->pages,'$obj->img')");
		$bd = null;
	}
	
    public function GetBooks(){
		$bd = $this->Connect();
		$rs = $bd->query("SELECT * FROM book");
		while($row = $rs->fetch(PDO::FETCH_OBJ)){
			$book_lista[] = array("id" => $row->id, "name" => $row->name, "category" => $row->category, "pages" => $row->pages, "img" => $row->img);
		}
		$bd = null;
		return $book_lista;
	}

	public function GetBookById($id){
		$bd = $this->Connect();
		$rs = $bd->query("SELECT id, name, category, pages, img FROM book WHERE id=".$id);
		$row = $rs->fetch(PDO::FETCH_OBJ);
		$book = new Book($row->id, $row->name, $row->category, $row->pages, $row->img);
		$bd = null;
		return $book;
	}
	
	public function UpdateBook($obj){
		$bd = $this->Connect();
		if($img){
			$bd->query("UPDATE book SET name = '$obj->name', category = '$obj->category', pages = $obj->pages, img = $obj->img) WHERE id=$obj->id");
		}else{
			$bd->query("UPDATE book SET name = '$obj->name', category = '$obj->category', pages = $obj->pages) WHERE id=$obj->id");			
		}
		$bd = null;
	}
	public function DeleteBook($id){
		$bd = $this->Connect();
		$bd->query("DELETE FROM book WHERE id=".$id);
		$bd = null;
	}
	
	function Connect(){
		$db = new PDO("mysql:host=localhost;port=54086;dbname=myDb", 'azure', '6#vWHD_$');
		return $db;
	}
}
?>