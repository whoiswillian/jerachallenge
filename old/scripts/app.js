var xhttp = new XMLHttpRequest();
function insereLivro(nome, tipo, paginas, img){
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			document.getElementById("insertForm").reset();
			clearTable("listaLivro");
			clearTable("remindersList");
			getBooks();
		}
	};
	xhttp.open("POST", "http://jerachallenge.azurewebsites.net/api/", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	if(img){
		xhttp.send("action=insert_book&name="+nome+"&category="+tipo+"&pages="+paginas+"&img="+img);
	}else{
		xhttp.send("action=insert_book&name="+nome+"&category="+tipo+"&pages="+paginas);			
	}
}
function insertReminder(hour, day, bookId){
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			document.getElementById("insertReminder").reset();
			document.getElementById("insertModal").style.display = "none";
			clearTable("listaLivro");
			clearTable("remindersList");
			getBooks();
    	}
	};
	xhttp.open("POST", "http://jerachallenge.azurewebsites.net/api/", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("action=insert_reminder&hour="+hour+"&day="+day+"&bookId="+bookId);
}
function atualizaLivro(id, nome, tipo, paginas, img){
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
		    document.getElementById("updateForm").reset();
			getBooks();
    	}
	};
	xhttp.open("POST", "http://jerachallenge.azurewebsites.net/api/", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	if(img){
		xhttp.send("action=update_book&nome="+nome+"&tipo="+tipo+"&paginas="+paginas+"&img="+img+"&id="+id);
	}else{
		xhttp.send("action=update_book&nome="+nome+"&tipo="+tipo+"&paginas="+paginas+"&id="+id);			
	}
}
function deletaLivro(id){
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			clearTable("listaLivro");
			clearTable("remindersList");
		    getBooks();
    	}
	};
	xhttp.open("GET", "http://jerachallenge.azurewebsites.net/api/?action=delete_book&id="+id, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send();
}
function deleteReminder(id){
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			clearTable("listaLivro");
			clearTable("remindersList");			
		    getBooks();
    	}
	};
	xhttp.open("GET", "http://jerachallenge.azurewebsites.net/api/?action=delete_reminder&id="+id, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send();
}
function getBookById(id){
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			var book = JSON.parse(this.responseText);
		   document.getElementById("itensModalTitle").innerHTML = "Livros";
		   document.getElementById("itensModalBody").innerHTML = "<div id='tempDiv' class='w3-center w3-margin'><table id='tempTable' class='w3-col s12 m12 l12 w3-container w3-center w3-margin-bottom'></table></div>";
		   document.getElementById("tempTable").innerHTML = "<tr class='w3-dark-gray'><td colspan='2'>"+book.name+"</td></tr>";
		   document.getElementById("tempTable").innerHTML += "<tr><td>Categoria</td><td> "+book.category+"</td></tr>";
		   document.getElementById("tempTable").innerHTML += "<tr><td>Paginas</td><td> "+book.pages+"<td></tr>";
		   document.getElementById("tempDiv").innerHTML += "<table id='tempSubTable' class='w3-col s12 m12 l12 w3-container w3-center w3-margin-bottom'></table>";
		   document.getElementById("tempSubTable").innerHTML = "<tr class='w3-dark-gray'><td colspan='2'>Lembretes</td></tr>";
		   getRemindersByBookId(book.id);
		   document.getElementById("itensModal").style.display = "block";
    	}
	};
	xhttp.open("GET", "http://jerachallenge.azurewebsites.net/api/?action=get_book_by_id&id="+id, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send();
}
function getReminderById(id){
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			var reminder = JSON.parse(this.responseText);
		   document.getElementById("itensModalTitle").innerHTML = "Lembrete";
		   document.getElementById("itensModalBody").innerHTML = "<div id='tempDiv' class='w3-center w3-margin'><table id='tempTable' class='w3-col s12 m12 l12 w3-container w3-center w3-margin-bottom'></table></div>";
		   document.getElementById("tempTable").innerHTML = "<tr class='w3-dark-gray'><td colspan='2'>"+reminder.bookId+"</td></tr>";
		   document.getElementById("tempTable").innerHTML += "<tr><td>Hora</td><td> "+reminder.hour+"</td></tr>";
		   document.getElementById("tempTable").innerHTML += "<tr><td>data</td><td> "+reminder.day+"<td></tr>";
    	}
	};
	xhttp.open("GET", "http://jerachallenge.azurewebsites.net/api/?action=get_reminder_by_id&id="+id, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send();
}
function getRemindersByBookId(id){
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			var i = 0;
			var reminder = JSON.parse(this.responseText);
			document.getElementById("tempSubTable").innerHTML += "<tr onclick='document.getElementById(\"bookId\").value = "+id+"; document.getElementById(\"insertModal\").style.display = \"block\";document.getElementById(\"itensModal\").style.display = \"none\";	' style='cursor: pointer;'><td colspan='2'>Adcionar lembrete</td></tr>";		
			if(reminder != null){
				while(i < reminder.length){
					document.getElementById("tempSubTable").innerHTML += "<tr><td>Hora</td><td>"+reminder[i].hour+"</td></tr>";
					document.getElementById("tempSubTable").innerHTML += "<tr><td>Dia</td><td>"+reminder[i].day+"</td></tr>";
					i++;
				}
			}else{
				document.getElementById("tempSubTable").innerHTML += "<tr><td>Sem lembretes</td></tr>";
			}
    	}
	};
	xhttp.open("GET", "http://jerachallenge.azurewebsites.net/api/?action=get_reminder_list_by_book_id&id="+id, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send();
}
function getBooks(){
	xhttp.open("GET", "http://jerachallenge.azurewebsites.net/api/?action=get_book_list", true);
	xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
			try{
				var i = 0;
				var list = JSON.parse(this.responseText);
				var tableLivro = document.getElementById("listaLivro");
				for(i = 0; i < list.length; i++){
					var row = tableLivro.insertRow(-1);
					row.innerHTML = '<td style="cursor: pointer;" onclick="getBookById('+list[i].id+')">'+list[i].name+'</td><td>'+list[i].category+'</td><td>'+list[i].pages+'</td><td class="w3-center"><div class="material-icons" onclick="document.getElementById(\'bookId\').value = '+list[i].id+'; document.getElementById(\'insertModal\').style.display = \'block\';" style="cursor: pointer;">alarm</div></td><td class="w3-center"><div class="material-icons" onclick="deletaLivro('+list[i].id+')" style="cursor: pointer;">delete</div></td>';
					row.classList.add("w3-hover-shadow");
				}
				getReminders();
			}catch(err){
				console.log("err msg = "+ err);
			}
	    };
	};
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send();
}

function getReminders(){
	xhttp.open("GET", "http://jerachallenge.azurewebsites.net/api/?action=get_reminder_list", true);
	xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
			try{
				var i = 0;
				var remindersList = JSON.parse(this.responseText);
				var remindersTable = document.getElementById("remindersList");
				if(remindersList != null && remindersList != undefined && remindersList.length > 0){
					for(i = 0; i < remindersList.length; i++){
						var row = remindersTable.insertRow(-1);
						row.innerHTML = '<td style="cursor: pointer;" onclick="getReminderById('+remindersList[i].id+')">'+remindersList[i].hour+'</td><td style="cursor: pointer;" onclick="getReminderById('+remindersList[i].id+')">'+remindersList[i].day+'</td><td>'+remindersList[i].bookId+'</td><td class="w3-center"><div class="material-icons" onclick="deleteReminder('+remindersList[i].id+')" style="cursor: pointer;">delete</div></td>';
						row.classList.add("w3-hover-shadow");
					}
				}
			}catch(err){
				console.log("err msg = "+ err);
			}
	    };
	};
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send();
}

function openCity(cityName, elmnt, color) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].classList.remove("w3-indigo", "w3-teal", "w3-red");
    }

    document.getElementById(cityName).style.display = "block";

    elmnt.classList.add("w3-"+color);
}
function clearTable(tableId) {
	var del = document.getElementById(tableId);
	if(del.getElementsByTagName('td').length > 0){
		del.deleteRow(1);
		clearTable(tableId);
	}
}