<form class="w3-container w3-form w3-center w3-xlarge w3-card-4 w3-padding w3-white w3-text-indigo" id="insertForm">
	<input id="nomeLivro" class="w3-input w3-center w3-margin-top" type="text" placeholder="Nome do livro"/>
	<input id="tipoLivro" class="w3-input w3-center" type="text" placeholder="Tipo do livro"/>
	<input id="pagNum" class="w3-input w3-center" type="number" placeholder="Quantidades de paginas"/>
	<input id="pic" class="w3-input w3-center w3-medium w3-margin-bottom" type="file" placeholder="Quantidades de paginas"/>
	<input id="btnGen" type="button" class="w3-btn w3-indigo w3-input w3-col s12 m12 l12" value="Inserir" onclick="insereLivro(document.getElementById('nomeLivro').value, document.getElementById('tipoLivro').value, document.getElementById('pagNum').value, document.getElementById('pic').value);"/>
</form>