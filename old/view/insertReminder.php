<form class="w3-container w3-form w3-center w3-xlarge" id="insertReminder">
	<input id="hour" class="w3-input w3-indigo w3-center w3-margin-bottom" type="time" placeholder="Nome do livro" required/>
	<input id="date" class="w3-input w3-indigo w3-center w3-margin-bottom w3-margin-top" type="date" placeholder="Tipo do livro"/>
	<input id="bookId" class="w3-input w3-center w3-indigo" type="hidden" placeholder="Quantidades de paginas"/>
	<input id="btnGen" type="button" class="w3-btn w3-white w3-text-indigo w3-input w3-margin-bottom w3-margin-top" value="Inserir" onclick="insertReminder(document.getElementById('hour').value, document.getElementById('date').value, document.getElementById('bookId').value);"/>
</form>
