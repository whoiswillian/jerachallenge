<div id="List" class="tabcontent w3-teal" style="height:calc(100% - 78px); position: fixed; overflow: auto; width: 100%; left: 0ps; padding: 0px;">
  <?php 
      include "view/viewAllBooks.php"
  ?>
</div>

<div id="Paris" class="tabcontent w3-indigo" style="height:calc(100% - 78px); position: fixed; overflow: auto; width: 100%; left: 0ps; padding: 0px;">
   <div class="w3-indigo w3-display-middle"><?php include "view/insertBook.php"?></div>
</div>

<div id="Tokyo" class="tabcontent w3-red" style="height:calc(100% - 78px); position: fixed; overflow: auto; width: 100%; left: 0ps; padding: 0px;">
  <?php
    include "view/viewAllReminders.php"
  ?>
</div>
<div class="w3-bottom" style="height: 78px">
  <button class="tablink w3-btn w3-col s4 m4 l4" style="height: 100%" onclick="openCity('List', this, 'teal')" id="defaultOpen"><div class="material-icons" style="font-size:48px">list</div></button>
  <button class="tablink w3-btn w3-col s4 m4 l4" style="height: 100%" onclick="openCity('Paris', this, 'indigo')"><div class="material-icons" style="font-size:48px">library_add</div></button>
  <button class="tablink w3-btn w3-col s4 m4 l4" style="height: 100%" onclick="openCity('Tokyo', this, 'red')"><div class="material-icons" style="font-size:48px">playlist_add_check</div></button>
</div>
<script>
  document.getElementById("defaultOpen").click();
</script>